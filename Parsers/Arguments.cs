﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CafiineRM.Parsers
{
    class Arguments
    {
        public static Dictionary<string, string> ParseArguments(Dictionary<string, string> arguments)
        {
            Dictionary<string, string> parsedArguments = new Dictionary<string, string>();

            // Port from Arguments
            string port;
            if(arguments.TryGetValue("PORT", out port)) { parsedArguments.Add("port", port); }
            else { parsedArguments.Add("port", "7332"); }

            // Relative Data Directory
            string data;
            if (arguments.TryGetValue("DATA", out data)) { parsedArguments.Add("data", data); }
            else { parsedArguments.Add("data", "data"); }

            // Relative Logs Directory
            string logs;
            if (arguments.TryGetValue("LOGS", out logs)) { parsedArguments.Add("logs", logs); }
            else { parsedArguments.Add("logs", "logs"); }

            // Debugger Mode
            string debug = "0";
            if (arguments.TryGetValue("DEBUG", out debug)) {
                if(!debug.Equals("0") || !debug.Equals("1"))
                {
                    debug = "1";
                }
                parsedArguments.Add("debug", debug);
            }
            else { parsedArguments.Add("debug", "0"); }

            return parsedArguments;
        }
    }
}
