﻿using System.Collections.Generic;
using System.IO;

namespace CafiineRM.Storage
{
    /// <summary>
    /// Represents a directory stored directly in the file system, containing game packs or other sub directories.
    /// </summary>
    internal class RootStorageDirectory : StorageDirectory
    {
        // ---- MEMBERS ------------------------------------------------------------------------------------------------

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="RootStorageDirectory"/> class for the given
        /// <see cref="DirectoryInfo"/>.
        /// </summary>
        /// <param name="directoryInfo">The directory to represent.</param>
        internal RootStorageDirectory(DirectoryInfo directoryInfo)
            : base(directoryInfo.Name)
        {
            DirectoryInfo = directoryInfo;

            // Create a dictionary to remember instantiated game packs (these are kept for the root directory).
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the represented <see cref="DirectoryInfo"/>.
        /// </summary>
        internal DirectoryInfo DirectoryInfo
        {
            get;
            private set;
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the child directories in this directory.
        /// </summary>
        /// <returns>The list of child directories.</returns>
        internal override IEnumerable<StorageDirectory> GetDirectories()
        {
            // Read the raw child directories.
            foreach (DirectoryInfo subDirectory in DirectoryInfo.GetDirectories())
            {
                if (!subDirectory.Attributes.HasFlag(FileAttributes.Hidden))
                {
                    yield return new RawStorageDirectory(subDirectory);
                }
            }
        }

        /// <summary>
        /// Returns the files in this directory.
        /// </summary>
        /// <returns>The list of files.</returns>
        internal override IEnumerable<StorageFile> GetFiles()
        {
            // Read the files (which are not game packs).
            foreach (FileInfo file in DirectoryInfo.GetFiles())
            {
                if (!file.Attributes.HasFlag(FileAttributes.Hidden))
                {
                    yield return new RawStorageFile(file);
                }
            }
        }
    }
}
