﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace CafiineRM.Managers
{
    class Cacher
    {
        private Thread thread;
        private List<string> systemCache = new List<string>();
        private List<string> bufferCache = new List<string>();
        private string titleId;
        private string cachePath;

        public Cacher(string cachePath, string titleId)
        {
            this.titleId = titleId;
            this.cachePath = cachePath;
            Directory.CreateDirectory(this.cachePath);

            thread = new Thread(HandleT);
            thread.Start();
        }

        public void TerminateThread()
        {
            try
            {
                thread.Abort();
            }
            catch (Exception ex)
            {
                if (Program.debugMode.Equals("1"))
                {
                    Program.Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", ex.StackTrace);
                }
            }
        }

        private void HandleT()
        {
            while(true)
            {
                Task.Run(() =>
                {
                    try
                    {
                        string[] lines;
                        try
                        {
                            lines = File.ReadAllLines(Path.GetFullPath(this.cachePath) + Path.DirectorySeparatorChar + "cache-" + this.titleId);
                        }
                        catch
                        {
                            lines = new string[0];
                        }
                        foreach (string s in lines)
                        {
                            if (systemCache.Contains(s) != true)
                            {
                                systemCache.Add(s);
                            }
                        }

                        foreach (string s in bufferCache)
                        {
                            if (systemCache.Contains(s) != true)
                            {
                                systemCache.Add(s);
                            }
                        }

                        using (StreamWriter file = new StreamWriter(@"" + Path.GetFullPath(this.cachePath) + Path.DirectorySeparatorChar + "cache-" + this.titleId))
                        {
                            foreach (string line in systemCache)
                            {
                                if (line != "" && line != " ")
                                {
                                    file.WriteLine(line);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.Log.Write(ConsoleColor.DarkRed, "Cacher", $"Iteration Skipped: " + ex.Message);
                        if (Program.debugMode.Equals("1"))
                        {
                            Program.Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", ex.StackTrace);
                        }
                    }
                });
                Thread.Sleep(10000);
            }
        }

        public bool IsIgnored(string filePath)
        {
            return this.systemCache.Contains(filePath);
        }

        public void AddToQueue(string filePath)
        {
            if (this.bufferCache.Contains(filePath) != true)
            {
                this.bufferCache.Add(filePath);
            }
        }
    }
}
