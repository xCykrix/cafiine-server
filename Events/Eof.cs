﻿using Syroot.BinaryData;
using System;
using System.Collections.Generic;
using System.IO;

namespace CafiineRM.Events
{
    public class Eof
    {
        internal static void Handle(BinaryDataReader reader, BinaryDataWriter writer, string logPrefix, string gameTitle, Stream[] fileStreams)
        {
            // Read the message parameters.
            int fileDescriptor = reader.ReadInt32();

            if ((fileDescriptor & 0x0FFF00FF) == 0x0FFF00FF)
            {
                // Get the stream of the file which position is queried to be at the end of the file.
                int handle = (fileDescriptor >> 8) & 0xFF;
                Stream fileStream = fileStreams[handle];
                if (fileStream == null)
                {
                    // The information cannot be retrieved as the file is not open.
                    Program.Log.Write(ConsoleColor.Red, logPrefix,
                        $"Cannot retrieve EOF of non-open file (handle={handle})");
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(-38);
                }
                else
                {
                    // Respond to Cafiine whether the stream reached the end of the file (-5) or not (0).
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(fileStream.Position == fileStream.Length ? -5 : 0);
                }
            }
            else
            {
                writer.Write((byte)ClientResponse.Normal);
            }
        }

        private enum ClientResponse : byte
        {
            Normal = 0xFF,
            Special = 0xFE
        }
    }
}
