﻿using CafiineRM.Managers;
using CafiineRM.Parsers;
using Syroot.BinaryData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace CafiineRM.Events
{
    public class StatFile
    {
        internal static void Handle(BinaryDataReader reader, BinaryDataWriter writer, string logPrefix, string gameTitle, Stream[] fileStreams, uint[] tIDParts)
        {
            // Read the message parameters.
            int fileDescriptor = reader.ReadInt32();

            if ((fileDescriptor & 0x0FFF00FF) == 0x0FFF00FF)
            {
                // Get the stream of the file which information is requested.
                int handle = (fileDescriptor >> 8) & 0xFF;
                Stream fileStream = fileStreams[handle];
                if (fileStream == null)
                {
                    // The information could not be retrieved because the file is not open.
                    Program.Log.Write(ConsoleColor.Red, logPrefix,
                        $"Cannot retrieve non-open file info (handle={handle})");
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(-38);
                    writer.Write(0);
                }
                else
                {
                    // Create the file information structure.
                    FSStat fileStats = new FSStat();
                    fileStats.Flags = FSStatFlag.None;
                    fileStats.Permission = 0x400;

                    fileStats.Owner = tIDParts[1];
                    fileStats.Group = 0x101E;
                    fileStats.FileSize = (uint)fileStream.Length;
                    // Send the file information to Cafiine.
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(0);
                    writer.Write(Marshal.SizeOf(fileStats));
                    writer.Write(fileStats);
                }
            }
            else
            {
                writer.Write((byte) ClientResponse.Normal);
            }
        }

        private enum ClientResponse : byte
        {
            Normal = 0xFF,
            Special = 0xFE
        }
    }
}
