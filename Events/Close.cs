﻿using Syroot.BinaryData;
using System;
using System.Collections.Generic;
using System.IO;

namespace CafiineRM.Events
{
    public class Close
    {
        internal static void Handle(BinaryDataReader reader, BinaryDataWriter writer, string logPrefix, string gameTitle, Stream[] fileStreams)
        {
            // Read the message parameters.
            int fileDescriptor = reader.ReadInt32();

            if ((fileDescriptor & 0x0FFF00FF) == 0x0FFF00FF)
            {
                // Get the stream of the file to be closed.
                int handle = (fileDescriptor >> 8) & 0xFF;
                Stream fileStream = fileStreams[handle];
                if (fileStream == null)
                {
                    // The file could not be closed because it was never open.
                    if (Program.debugMode.Equals("1"))
                    {
                        Program.Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", $"Failed to Close File Reference (FH:{handle})");
                    }
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(-38);
                }
                else
                {
                    // Close the requested file and clear the slot in the file handle array.
                    if (fileStream.GetType() == typeof(FileStream))
                    {
                        if (Program.debugMode.Equals("1"))
                        {
                            Program.Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", $"Closed File Reference (FH:{handle})");
                        }
                    }
                    fileStream.Dispose();
                    fileStreams[handle] = null;
                    // Send a response that closing the file was successful.
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(0);
                }
            }
        }

        private enum ClientResponse : byte
        {
            Normal = 0xFF,
            Special = 0xFE
        }
    }
}
