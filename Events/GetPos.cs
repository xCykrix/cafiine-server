﻿using Syroot.BinaryData;
using System;
using System.Collections.Generic;
using System.IO;

namespace CafiineRM.Events
{
    public class GetPos
    {
        internal static void Handle(BinaryDataReader reader, BinaryDataWriter writer, string logPrefix, string gameTitle, Stream[] fileStreams)
        {
            // Read the message parameters.
            int fileDescriptor = reader.ReadInt32();

            if ((fileDescriptor & 0x0FFF00FF) == 0x0FFF00FF)
            {
                // Get the stream of the file which position is queried.
                int handle = (fileDescriptor >> 8) & 0xFF;
                Stream fileStream = fileStreams[handle];
                if (fileStream == null)
                {
                    // The position cannot be retrieved as the file is not open.
                    Program.Log.Write(ConsoleColor.Red, logPrefix,
                        $"Cannot get position of non-open file (handle={handle})");
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(-38);
                    writer.Write(0);
                }
                else
                {
                    // Return the file stream position back to Cafiine.
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(0);
                    writer.Write((int)fileStream.Position);
                }
            }
            else
            {
                writer.Write((byte)ClientResponse.Normal);
            }
        }

        private enum ClientResponse : byte
        {
            Normal = 0xFF,
            Special = 0xFE
        }
    }
}
