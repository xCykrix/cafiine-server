﻿using Syroot.BinaryData;
using System;
using System.Collections.Generic;
using System.IO;

namespace CafiineRM.Events
{
    public class SetPos
    {
        internal static void Handle(BinaryDataReader reader, BinaryDataWriter writer, string logPrefix, string gameTitle, Stream[] fileStreams)
        {
            // Read the message parameters.
            int fileDescriptor = reader.ReadInt32();
            int position = reader.ReadInt32();

            if ((fileDescriptor & 0x0FFF00FF) == 0x0FFF00FF)
            {
                // Get the stream of the file to seek in.
                int handle = (fileDescriptor >> 8) & 0xFF;
                Stream fileStream = fileStreams[handle];
                if (fileStream == null)
                {
                    // The file could not be seeked in because it is not open.
                    Program.Log.Write(ConsoleColor.Red, logPrefix, $"Cannot seek non-open file (handle={handle})");
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(-38);
                }
                else
                {
                    // Set the position.
                    fileStream.Position = position;
                    // Send a response that we could seek successfully.
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(0);
                }
            }
            else
            {
                writer.Write((byte)ClientResponse.Normal);
            }
        }

        private enum ClientResponse : byte
        {
            Normal = 0xFF,
            Special = 0xFE
        }
    }
}
