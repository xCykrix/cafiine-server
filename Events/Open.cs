﻿using CafiineRM.Managers;
using CafiineRM.Storage;
using Syroot.BinaryData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CafiineRM.Events
{
    class Open
    {
        internal static void Handle(BinaryDataReader reader, BinaryDataWriter writer, string logPrefix, string gameTitle, Stream[] fileStreams)
        {
            int pathLength = reader.ReadInt32();
            int modeLength = reader.ReadInt32();
            string path = reader.ReadString(StringDataFormat.ZeroTerminated, Encoding.ASCII);
            string mode = reader.ReadString(StringDataFormat.ZeroTerminated, Encoding.ASCII);

            string fullPath = GetServerPath(gameTitle, path);

            Cacher cache;
            TCPServer.Cache.TryGetValue(gameTitle, out cache);
            if (cache.IsIgnored(path) == true)
            {
                writer.Write((byte) ClientResponse.Normal);
                return;
            }

            StorageFile file;
            if ((file = TCPServer.Storage.GetFile(gameTitle + path)) != null)
            {
                int handle = -1;
                for (int i = 0; i < fileStreams.Length; i++)
                {
                    if (fileStreams[i] == null)
                    {
                        handle = i;
                        break;
                    }
                }
                if (handle == -1)
                {
                    if (Program.debugMode.Equals("1"))
                    {
                        Program.Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", "ILLEGAL FILEHANDLE STATE, UPLOAD FAILED");
                    }
                    writer.Write((byte) ClientResponse.Normal);
                    return;
                }

                if (file.GetType() == typeof(RawStorageFile))
                {
                    Program.Log.Write(ConsoleColor.DarkGreen, logPrefix,
                        $"> Inject: '{path}' (Mode:{mode.ToUpper()}) (FH:{handle})");
                }
                fileStreams[handle] = file.GetStream();

                writer.Write((byte) ClientResponse.Special);
                writer.Write(0);
                writer.Write(0x0FFF00FF | (handle << 8));
            }
            else
            {
                cache.AddToQueue(path);
                Program.Log.Write(ConsoleColor.DarkYellow, logPrefix,
                    $"> Ignore: '{path}' (Mode:{mode.ToUpper()}) (FH:null)");
                writer.Write((byte)ClientResponse.Normal);
            }
        }

        // Internal Pathing Function
        private static string GetServerPath(string _titleID, string path)
        {
            path = path.TrimStart('/').Replace('/', Path.DirectorySeparatorChar);
            return Path.Combine(TCPServer.dataPath, _titleID, path);
        }

        private enum ClientResponse : byte
        {
            Normal = 0xFF,
            Special = 0xFE
        }
    }
}
