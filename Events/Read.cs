﻿using Syroot.BinaryData;
using System;
using System.Collections.Generic;
using System.IO;

namespace CafiineRM.Events
{
    public class Read
    {
        internal static void Handle(BinaryDataReader reader, BinaryDataWriter writer, string logPrefix, string gameTitle, Stream[] fileStreams)
        {
            // Read the message parameters.
            int size = reader.ReadInt32();
            int count = reader.ReadInt32();
            int fileDescriptor = reader.ReadInt32();

            if ((fileDescriptor & 0x0FFF00FF) == 0x0FFF00FF)
            {
                // Get the file stream to read from.
                int handle = (fileDescriptor >> 8) & 0xFF;
                Stream fileStream = fileStreams[handle];
                if (fileStream == null)
                {
                    // The file could not be read because it was not opened before.
                    if (Program.debugMode.Equals("1"))
                    {
                        Program.Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", $"Unable to Reference File (FH:{handle})" );
                    }
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(-19);
                    writer.Write(0);
                }
                else
                {
                    // Read in the file data and send it to Cafiine.
                    byte[] buffer = new byte[size * count];
                    int readBytes = fileStream.Read(buffer, 0, buffer.Length);
                    writer.Write((byte)ClientResponse.Special);
                    writer.Write(readBytes / size);
                    writer.Write(readBytes);
                    writer.Write(buffer, 0, readBytes);
                    // Check if the sent data could be accepted.
                    if (reader.ReadByte() != (byte)ClientResponse.OK)
                    {
                        throw new InvalidDataException("Unacceptable Data");
                    }
                }
            }
            else
            {
                writer.Write((byte)ClientResponse.Normal);
            }
        }

        private enum ClientResponse : byte
        {
            Normal = 0xFF,
            OK = 0x03,
            Special = 0xFE
        }
    }
}
