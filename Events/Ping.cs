﻿using Syroot.BinaryData;
using System;
using System.Collections.Generic;
using System.IO;

namespace CafiineRM.Events
{
    public class Ping
    {
        internal static void Handle(BinaryDataReader reader, BinaryDataWriter writer, string logPrefix, string gameTitle, Stream[] fileStreams)
        {
            // Read the message parameters.
            int value1 = reader.ReadInt32();
            int value2 = reader.ReadInt32();

            Program.Log.Write(ConsoleColor.DarkGray, logPrefix, $"Pinging Client-Server...");
        }

        private enum ClientResponse : byte
        {
            Normal = 0xFF,
            Special = 0xFE
        }
    }
}
