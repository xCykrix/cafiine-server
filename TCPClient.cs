﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using CafiineRM.Events;
using CafiineRM.Managers;
using Syroot.BinaryData;

namespace CafiineRM
{
    class TCPClient
    {
        private TCPServer tcpServer;
        private TcpClient tcpClient;
        private string tcpRemote;
        private Cacher cacher;

        private uint[] IDParts;
        private string ID;
        private Stream[] FileStreams;
        private BinaryDataReader tcpReader;
        private BinaryDataWriter tcpWriter;

        public TCPClient(TCPServer tcpServer, TcpClient tcpClient)
        {
            this.tcpServer = tcpServer;
            this.tcpClient = tcpClient;
            this.tcpRemote = (tcpClient.Client.RemoteEndPoint as IPEndPoint).Address.ToString();

            this.FileStreams = new Stream[512];
        }

        internal void Handle()
        {
            Thread thread = new Thread(HandleT);
            thread.Start();
        }

        private void HandleT()
        {
            try
            {
                using (NetworkStream net = tcpClient.GetStream())
                {
                    tcpReader = new BinaryDataReader(net);
                    tcpReader.ByteOrder = ByteOrder.BigEndian;
                    tcpWriter = new BinaryDataWriter(net);
                    tcpWriter.ByteOrder = ByteOrder.BigEndian;

                    IDParts = tcpReader.ReadUInt32s(4);
                    ID = $"{IDParts[0]:X8}-{IDParts[1]:X8}";

                    if(TCPServer.Storage.GetDirectory(ID) != null)
                    {
                        Program.Log.Write(ConsoleColor.DarkGreen, "TitleID Lookup", $"Mods Found: {ID}");

                        if (!TCPServer.Cache.ContainsKey(ID))
                        {
                            TCPServer.Cache.Add(ID, new Cacher(TCPServer.dataPath, ID));
                        }

                        TCPServer.Cache.TryGetValue(ID, out cacher);
                        tcpWriter.Write((byte) ClientCommand.Special);
                    }
                    else
                    {
                        Program.Log.Write(ConsoleColor.DarkRed, "TitleID Lookup", $"No Mods Found: {ID}");
                        tcpWriter.Write((byte) ClientCommand.Normal);
                        return;
                    }

                    while(true)
                    {
                        ClientCommand command = (ClientCommand) tcpReader.ReadByte();
                        switch(command)
                        {
                            case ClientCommand.Open: Open.Handle(tcpReader, tcpWriter, tcpRemote, ID, FileStreams); break;
                            case ClientCommand.Read: Read.Handle(tcpReader, tcpWriter, tcpRemote, ID, FileStreams); break;
                            case ClientCommand.Close: Close.Handle(tcpReader, tcpWriter, tcpRemote, ID, FileStreams); break;
                            case ClientCommand.SetPos: SetPos.Handle(tcpReader, tcpWriter, tcpRemote, ID, FileStreams); break;
                            case ClientCommand.StatFile: StatFile.Handle(tcpReader, tcpWriter, tcpRemote, ID, FileStreams, IDParts); break;
                            case ClientCommand.Eof: Eof.Handle(tcpReader, tcpWriter, tcpRemote, ID, FileStreams); break;
                            case ClientCommand.GetPos: GetPos.Handle(tcpReader, tcpWriter, tcpRemote, ID, FileStreams); break;
                            case ClientCommand.Ping: Ping.Handle(tcpReader, tcpWriter, tcpRemote, ID, FileStreams); break;
                        }
                    }
                }
            }
            catch(IOException ex)
            {
                Program.Log.Write(ConsoleColor.DarkRed, $"CafiineRM [{tcpRemote}]", $"File Exception Generated: {ex.Message}");
                if (Program.debugMode.Equals("1"))
                {
                    Program.Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", ex.StackTrace);
                }
            }
            catch (Exception ex)
            {
                Program.Log.Write(ConsoleColor.DarkRed, $"CafiineRM [{tcpRemote}]", $"Unknown Exception Generated: {ex.Message}");
                if (Program.debugMode.Equals("1"))
                {
                    Program.Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", ex.StackTrace);
                }
            }
            finally
            {
                foreach (Stream stream in FileStreams)
                {
                    if(stream != null)
                    {
                        try
                        {
                            stream.Dispose();
                            stream.Close();
                        }
                        catch(Exception ex)
                        {
                            if (Program.debugMode.Equals("1"))
                            {
                                Program.Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", ex.StackTrace);
                            }
                        }
                    }
                }
                cacher.TerminateThread();
            }
        }

        private enum ClientCommand : byte
        {
            Open = 0x00,
            Read = 0x01,
            Close = 0x02,
            OK = 0x03,
            SetPos = 0x04,
            StatFile = 0x05,
            Eof = 0x06,
            GetPos = 0x07,
            Request = 0x08,
            RequestSlow = 0x09,
            Ping = 0x0C,
            Special = 0xFE,
            Normal = 0xFF
        }
    }

}