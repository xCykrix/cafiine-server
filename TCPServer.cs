﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using CafiineRM.Managers;
using CafiineRM.Storage;

namespace CafiineRM
{
    class TCPServer
    {
        public static StorageSystem Storage;
        public static Dictionary<string, Cacher> Cache;
        public static string dataPath;
        public static string logPath;

        private IPAddress ipAddress;
        private int port;

        public TCPServer(IPAddress ipAddress, int port, string dataPath, string logPath)
        {
            this.ipAddress = ipAddress;
            this.port = port;
            TCPServer.dataPath = dataPath;
            TCPServer.logPath = logPath;

            Storage = new StorageSystem(TCPServer.dataPath);
            Cache = new Dictionary<string, Cacher>();

            Program.Log.Write(ConsoleColor.Green, "CafiineRM-TCP", "Listening for WiiU Client(s)...");
        }

        internal async Task Run()
        {
            TcpListener listener = new TcpListener(ipAddress, port);

            listener.Server.SendBufferSize = 4 * 1024;
            listener.Start();
            
            while(true)
            {
                TCPClient client = new TCPClient(this, await listener.AcceptTcpClientAsync());
                client.Handle();
            }

        }

    }
}
