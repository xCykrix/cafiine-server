﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using CafiineRM.Managers;
using CafiineRM.Parsers;

namespace CafiineRM
{
    internal static class Program
    {
        public static LogManager Log;
        public static Dictionary<string, List<string>> idSkipped = new Dictionary<string, List<string>>();

        private static IPAddress ipAddress = IPAddress.Any;
        private static int port;
        public static string debugMode;
        private static string dataPath;
        private static string logPath;

        private static void Main(string[] args)
        {
            Dictionary<string, string> arguments = ParameterParser.ParseToDictionary(args);
            Dictionary<string, string> pArguments = Arguments.ParseArguments(arguments);

            string _sPort;
            pArguments.TryGetValue("port", out _sPort);
            port = int.Parse(_sPort);
            pArguments.TryGetValue("data", out dataPath);
            pArguments.TryGetValue("logs", out logPath);
            pArguments.TryGetValue("debug", out debugMode);

            Log = new LogManager(logPath);

            Task.Run(async () =>
            {
                try
                {
                    Log.Write(ConsoleColor.Yellow, "CafiineRM-Core", "Listening on all interfaces for port " + port);
                    Log.Write(ConsoleColor.DarkYellow, "CafiineRM-Core",
                        $"Data Storage: '{Path.GetFullPath(dataPath)}'");
                    Log.Write(ConsoleColor.DarkYellow, "CafiineRM-Core",
                        $"Logs Storage: '{Path.GetFullPath(logPath)}'");
                    if (debugMode.Equals("1")) {
                        Log.Write(ConsoleColor.DarkYellow, "CafiineRM-Core",
                        $"Debugger Mode: 'ACTIVE'");
                    }


                    if (arguments.ContainsKey("?") || arguments.ContainsKey("HELP")) { PrintHelp(); }
                    else
                    {
                        TCPServer server = new TCPServer(ipAddress, port, dataPath, logPath);
                        await server.Run();
                    }

                }
                catch (Exception ex)
                {
                    Log.Write(ConsoleColor.DarkRed, "CafiineRM-Fatal", "Exception: " + ex.Message);
                    if (debugMode.Equals("1"))
                    {
                        Log.Write(ConsoleColor.DarkRed, "CafiineRM-Debug", ex.StackTrace);
                    }
                }
            }).Wait();
        }

        private static void PrintHelp()
        {
            Console.WriteLine("Communicate with WiiU Cafiine Client to Inject RAM Files.");
            Console.WriteLine();
            Console.WriteLine("CafiineRM.exe [/PORT=7332] [/DATA=DataPath] [/LOGS=LogsPath]");
            Console.WriteLine();
            Console.WriteLine("   --PORT | /PORT    The port under which the server will listen for incoming client");
            Console.WriteLine("                 	connections. Defaults to 7332.");
            Console.WriteLine("   --DATA | /DATA    The path to the game data directory containing either game packs");
            Console.WriteLine("                 	or title ID folders. Defaults to './data'");
            Console.WriteLine("   --LOGS | /LOGS    The path to the directory in which logs will be stored. Defaults");
            Console.WriteLine("                 	to './logs'");
        }
    }


}
